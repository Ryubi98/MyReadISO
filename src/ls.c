#include "commands.h"

static void print_ls(struct iso_dir *dir)
{
  if (dir->type == ISO_FILE_ISDIR)
    printf("d- ");
  else if (dir->type == ISO_FILE_HIDDEN)
    printf("-h ");
  else
    printf("-- ");

  printf("%9u ", dir->file_size.le);

  printf("%04d/%02d/%02d %02d:%02d ", 1900 + dir->date[0], dir->date[1],
         dir->date[2], dir->date[3], dir->date[4]);

  char *cpy = pointer(dir);

  if (!*cpy)
    printf(".\n");
  else if (*cpy == '\1')
    printf("..\n");
  else
  {
    char c;
    for (uint8_t i = 0; i < dir->idf_len; i++)
    {
      c = *(cpy + i);
      if (c == ';')
        break;
      putchar(c);
    }
    putchar('\n');
  }
}

void ls(struct iso_dir *dir)
{
  while (dir->dir_size > 0)
  {
    print_ls(dir);

    void *p = dir;
    char *cpy = p;
    p = cpy + dir->dir_size;
    dir = p;
  }
}
