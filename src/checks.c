#include "checks.h"

int check_file(struct iso_dir *dir, char *args)
{
  char *cpy = pointer(dir);

  if (!strcmp(args, ".") || !strcmp(args, ".."))
    return -1;
  else
  {
    int i = 0;
    int size = strlen(args);

    while (*(cpy + i) != ';' && i < size)
    {
      if (*(cpy + i) != *(args + i))
        return 0;
      i++;
    }

    if (i == size)
    {
      if (dir->type == ISO_FILE_ISDIR)
        return -1;
      return *(cpy + i) == ';';
    }
    return 0;
  }
}

int check_dir(struct iso_dir *dir, char *args)
{
  char *cpy = pointer(dir);

  if (!strcmp(args, "."))
    return 2;
  else if (!strcmp(args, ".."))
    return 3;
  else
  {
    int i = 0;
    int size = strlen(args);

    while (i < dir->idf_len && i < size)
    {
      if (*(cpy + i) != *(args + i))
        return 0;
      i++;
    }

    if (i == dir->idf_len && i == size)
    {
      if (dir->type == ISO_FILE_ISDIR)
        return 1;
    }
    return 0;
  }
}
