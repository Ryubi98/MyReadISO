#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "iso9660.h"
#include "commands.h"

#define BUFFER_SIZE 4096

static void extract_cmd_args(char *s, char *cmd, char *args)
{
  int i = 0;
  while (*(s + i) != '\n')
  {
    if (*(s + i) != ' ' && *(s + i) != '\t')
      break;
    i++;
  }

  if (*(s + i) == '\n')
    return;

  int j = 1;
  while (*(s + i + j) != '\n')
  {
    if (*(s + i + j) == ' ' || *(s + i + j) == '\t')
      break;
    j++;
  }

  strncpy(cmd, s + i, j);

  i += j;
  while (*(s + i) != '\n')
  {
    if (*(s + i) != ' ' && *(s + i) != '\t')
      break;
    i++;
  }

  if (*(s + i) != '\n')
  {

    int len = strlen(s) - 2;
    while (i < len)
    {
      if (*(s + len) != ' ' && *(s + len) != '\t')
        break;
      len--;
    }

    strncpy(args, s + i, len - i + 1);
  }
}

static int commands_manager(char *ptr, struct iso_prim_voldesc *prim,
                            struct iso_dir **current, char *cmd, char *args,
                            char *bin)
{
  if (!strcmp(cmd, "help"))
    help(args, bin);
  else if (!strcmp(cmd, "info"))
    info(prim, args, bin);
  else if (!strcmp(cmd, "ls"))
    ls(*current);
  else if (!strcmp(cmd, "cd"))
    cd(ptr, current, args, bin);
  else if (!strcmp(cmd, "tree"))
    tree(args, bin);
  else if (!strcmp(cmd, "get"))
    get(ptr, *current, args, bin);
  else if (!strcmp(cmd, "cat"))
    cat(ptr, *current, args, bin);
  else if (!strcmp(cmd, "pwd"))
    pwd(args, bin);
  else if (!strcmp(cmd, "quit"))
    return quit(args, bin);
  else if (strcmp(cmd, ""))
    fprintf(stderr, "%s: %s: unknown command\n", bin, cmd);

  return 0;
}

static void mini_shell(char *ptr, struct iso_prim_voldesc *prim,
                       char *bin, int tty)
{
  int quit = 0;
  char buff[BUFFER_SIZE];
  void *p = ptr + (prim->root_dir.data_blk.le * ISO_BLOCK_SIZE);
  struct iso_dir *current = p;

  while (!quit)
  {
    if (tty)
      printf("> ");

    char cmd[BUFFER_SIZE] = { 0 };
    char args[BUFFER_SIZE] = { 0 };

    if (!fgets(buff, BUFFER_SIZE, stdin))
      return;

    extract_cmd_args(buff, cmd, args); // extract cmd and args from buff

    // call fct with cmd + args
    quit = commands_manager(ptr, prim, &current, cmd, args, bin);
  }
}

static struct iso_prim_voldesc *check_iso(void *ptr, off_t size)
{
  off_t min_size = ISO_BLOCK_SIZE * ISO_PRIM_VOLDESC_BLOCK;
  min_size += sizeof(struct iso_prim_voldesc);

  if (size < min_size) // check the size
    return NULL;

  char *cpy = ptr;
  ptr = cpy + (ISO_BLOCK_SIZE * ISO_PRIM_VOLDESC_BLOCK);
  struct iso_prim_voldesc *prim = ptr;

  if (strncmp(prim->std_identifier, "CD001", 5)) // check the std_id
    return NULL;

  return prim;
}

int main(int argc, char *argv[])
{
  if (argc != 2) // wrong number of arguments
    errx(1, "usage: %s FILE", argv[0]);

  int fd_iso = open(argv[1], O_RDONLY); // open given file
  if (fd_iso < 0) // failed
    errx(1, "%s: No such file or directory", argv[1]);

  struct stat fs;
  if (fstat(fd_iso, &fs) != 0) // load file's stats
  {
    close(fd_iso);
    errx(1, "Fail reading iso stats");
  }

  // map given file with the right size
  void *ptr = mmap(NULL, fs.st_size, PROT_READ, MAP_PRIVATE, fd_iso, 0);
  if (ptr == MAP_FAILED) // failed
  {
    close(fd_iso);
    errx(1, "Fail mapping iso");
  }

  struct iso_prim_voldesc *prim;
  prim = check_iso(ptr, fs.st_size); // check if the file is a iso format
  if (!prim)
  {
    munmap(ptr, fs.st_size);
    close(fd_iso);
    errx(1, "This file is not iso format");
  }
  else
    mini_shell(ptr, prim, argv[0], isatty(0)); // start the program

  munmap(ptr, fs.st_size); // free map
  close(fd_iso); // close fd
  return 0;
}
