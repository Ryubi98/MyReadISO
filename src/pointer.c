#include "pointer.h"

char *pointer(struct iso_dir *dir)
{
  void *p = dir;
  char *cpy = p;
  p = cpy + sizeof(struct iso_dir);
  cpy = p;

  return cpy;
}
