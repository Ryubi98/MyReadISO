CC ?= gcc
CFLAGS = -Wall -Wextra -Werror -std=c99 -pedantic -g

DIR_SRC = src
SRC = $(wildcard $(DIR_SRC)/*.c)

BIN = my_read_iso

all: $(BIN)

$(BIN): $(SRC)
	$(CC) $(CFLAGS) -o $@ $^

test: $(BIN)
	./$(BIN) example.iso < tests/commands

clean:
	$(RM) $(BIN)

.PHONY: clean
