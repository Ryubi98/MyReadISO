#include "commands.h"

int quit(char *args, char *bin)
{
  if (*args)
  {
    fprintf(stderr, "%s: quit: command does not take an argument\n", bin);
    return 0;
  }

  return 1;
}
