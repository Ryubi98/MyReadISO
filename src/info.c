#include "commands.h"

void info(struct iso_prim_voldesc *prim, char *args, char *bin)
{
  if (*args)
    fprintf(stderr, "%s: info: command does not take an argument\n", bin);
  else
  {
    printf("System Identifier: %.32s\n", prim->syidf);
    printf("Volume Identifier: %.32s\n", prim->vol_idf);
    printf("Block count: %d\n", prim->vol_blk_count.le);
    printf("Block size: %d\n", prim->vol_blk_size.le);
    printf("Creation date: %.17s\n", prim->date_creat);
    printf("Application Identifier: %.128s\n", prim->app_idf);
  }
}
