#include "commands.h"
#include "checks.h"

static struct iso_dir *move_dir(char *ptr, struct iso_dir *dir, int check)
{
  void *new = NULL;
  char *cpy = NULL;

  switch (check)
  {
    case 1: // changing to a normal directory
      new = ptr + (dir->data_blk.le * ISO_BLOCK_SIZE);

      cpy = pointer(dir);

      printf("Changing to '");
      for (uint8_t i = 0; i < dir->idf_len; i++)
        putchar(*(cpy + i));
      printf("' directory\n");

      return new;
    case 2: // changing to .
      printf("Changing to '/' directory\n");
      return dir;
    case 3: // changing to ..
      printf("Changing to '/' directory\n");
      return dir;
  }
  return NULL;
}

void cd(char *ptr, struct iso_dir **current, char *args, char *bin)
{
  if (!*args)
  {
    void *p = ptr + (ISO_BLOCK_SIZE * ISO_PRIM_VOLDESC_BLOCK);
    struct iso_prim_voldesc *prim = p;
    p = ptr + (prim->root_dir.data_blk.le * ISO_BLOCK_SIZE);
    struct iso_dir *new = p;
    *current = new;

    printf("Changing to '/' directory\n");
    return ;
  }
  else
  {
    struct iso_dir *dir = *current;

    while (dir->dir_size > 0)
    {
      int check = check_dir(dir, args);
      if (check)
      {
        *current = move_dir(ptr, dir, check);
        return;
      }

      void *p = dir;
      char *cpy = p;
      p = cpy + dir->dir_size;
      dir = p;
    }

    fprintf(stderr, "%s: unable to find '%s' entry\n", bin, args);
  }
}
