#include "commands.h"

void pwd(char *args, char *bin)
{
  if (*args)
    fprintf(stderr, "%s: pwd: command does not take an argument\n", bin);
  else
    printf("/\n");
}
