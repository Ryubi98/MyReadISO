#ifndef COMMANDS_H
#define COMMANDS_H

#include <stdio.h>

#include "iso9660.h"
#include "pointer.h"

// display command help
void help(char *args, char *bin);

// display volume info
void info(struct iso_prim_voldesc *prim, char *args, char *bin);

// display the content of a directory
void ls(struct iso_dir *dir);

// change current directory
void cd(char *ptr, struct iso_dir **current, char *args, char *bin);

// display the tree of a directory
void tree(char *args, char *bin);

// copy file to local directory
void get(char *ptr, struct iso_dir *dir, char *args, char *bin);

// display file content
void cat(char *ptr, struct iso_dir *dir, char *args, char *bin);

// print current path
void pwd(char *args, char *bin);

// exit the program
int quit(char *args, char *bin);

#endif /* ! COMMANDS_H */
