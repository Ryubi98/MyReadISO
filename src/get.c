#include "commands.h"
#include "checks.h"

void get(char *ptr, struct iso_dir *dir, char *args, char *bin)
{
  if (!*args)
    fprintf(stderr, "%s: get: this function must take an argument\n", bin);
  else
  {
    while (dir->dir_size > 0)
    {
      int res = check_file(dir, args);
      if (res == 1)
      {
        ptr = ptr + (dir->data_blk.le * ISO_BLOCK_SIZE);

        FILE *f = fopen(args, "w+");
        int len = dir->file_size.le;
        for (int i = 0; i < len; i++)
          fputc(*(ptr + i), f);
        fclose(f);
        return;
      }
      else if (res == -1)
      {
        fprintf(stderr, "%s: entry '%s' is a directory\n", bin, args);
        return;
      }

      void *p = dir;
      char *cpy = p;
      p = cpy + dir->dir_size;
      dir = p;
    }

    fprintf(stderr, "%s: unable to find '%s' entry\n", bin, args);
  }
}
