#ifndef CHECKS_H
#define CHECKS_H

#include <string.h>

#include "iso9660.h"
#include "pointer.h"

int check_file(struct iso_dir *dir, char *args);

int check_dir(struct iso_dir *dir, char *args);

#endif /* ! CHECKS_H */
